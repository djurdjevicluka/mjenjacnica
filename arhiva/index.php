<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://kit.fontawesome.com/6cd93a92c8.js"></script>
<script>
	$(document).ready(function(){
		$('nav ul li').on('click',function(){
			var gdje =$(this).attr('rel');
			if(gdje!='pocetna'){
				$('html,body').animate({
					scrollTop: $('#'+gdje).offset().top-40
				},1000)
			}else {
				$('html,body').animate({
					scrollTop:0
				},1000)
			}
		});
	});
</script>
<link rel="stylesheet" href="main.css">
<link href="https://fonts.googleapis.com/css?family=Raleway&display=swap" rel="stylesheet">	
</head>
<body>

<header>
    <div class="menu">
            <i class="fas fa-bars fa-2x"></i>
    </div>
    <nav>
		<h1><img src="money-3614661_960_720.png" style="height: 25px; width:25px; align: center; padding-bottom: 1px"> Mjenjačnica Lipa</h1>
        <ul>
			<li rel="kontakt">Kontakt</li>
			<li rel="konverter">O nama</li>
			<li rel="pocetna">Početna</li>
        </ul>
    </nav>
</header>

<div id="pocetna" style="height:750px">
	<h1 id="naslov">Mjenjačnica Lipa<br><p id="podnaslov">dobrodošli na zvanični web sajt mjenjačnice</p></h1> 	
</div>
<br><br>
<div id="konverter" style="height:1000px">
	<table style="width:100%" class="tabela">
		<tr>
			<td align="center"><a class="medalja"><img src="icon/gold-medal-icon-png-25.jpg" style="height: 120px; width: 120px;"></a></td>
			<td align="center"><a class="struja"><img src="icon/lightning-icon.png" style="height: 120px; width:120px"></a></td> 
			<td align="center"><a class="graf"><img src="icon/1-512.png" style="height: 120px; width:120px"></a></td>
			<td align="center"><a class="krug"><img src="icon/e06fa7ed8b.png" style="height: 120px; width: 135px"></a></td>
		</tr>
		<tr><td>&nbsp;</td><td>&nbsp;</td></tr>
		<tr>
			<td align="center"><p id="a1">Pouzdanost</p></td>
			<td align="center"><p id="a2">Efikasno poslovanje</p></td>
			<td align="center"><p id="a3">Profesionalna podrška</p></td>
			<td align="center"><p id="a4">Isplativost</p></td>
		</tr>
	</table>
	<br><br><br><br><br>
	<p class="hdng" align="center">Pored jednostavnog i efikasnog pristupa mnoštvu stranih valuta, naša poslovnica svoje kupce svakodnevno snabdijeva dnevnom štampom, te im takoder nudi veliku ponudu raznih vrsta cigareta, duvana i slicno.</p>

</div>

<div id="karta" align=center>

	<iframe style="width: 70%" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2818.8356899357545!2d16.372610115543523!3d45.048556079098304!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4766d4ee95034ae3%3A0x360d978255cdc34c!2sMjenja%C4%8Dnica+Lipa!5e0!3m2!1shr!2sba!4v1565911927410!5m2!1shr!2sba" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
	
	<p id="kontakt">Kontakt<br><a href="tel:+38752751027">052/751-027</a></p>

</div>


</body>
</html>