<!DOCTYPE html>
<html>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<!--<script>
	$(document).ready(function{
		$('.nav li a').click(function(){
			if(location.pathname.replace(/^\//,'')==this.pathname.replace(/^\//,'') && location.hostname==this.hostname){
				var $target=$(this.hash);
				$target=$target.length && $target
				|| $('[name=' + this.hash.slice(1) + ']');
				if($target.length){
					var targetOffset = $target.offset().top;
					$('html,body').animate({scrollTop:targetOffset}, 1000);
					return false;
				}
			}
		});
	});
</script>-->
<script>
$(document).ready(function(){
  // Add smooth scrolling to all links
  $("a").on('click', function(event) {

    // Make sure this.hash has a value before overriding default behavior
    if (this.hash !== "") {
      // Prevent default anchor click behavior
      event.preventDefault();

      // Store hash
      var hash = this.hash;

      // Using jQuery's animate() method to add smooth page scroll
      // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 800, function(){

        // Add hash (#) to URL when done scrolling (default click behavior)
        window.location.hash = hash;
      });
    } // End if
  });
});
</script>
<style>
body {font-family: Arial, Helvetica, sans-serif;}

.navbar {
  width: 100%;
  background-color: #555;
  overflow: auto;
}

.navbar a {
  float: left;
  padding: 12px;
  color: white;
  text-decoration: none;
  font-size: 17px;
}

.navbar a:hover {
  background-color: #000;
}

.active {
  background-color: #4CAF50;
}

@media screen and (max-width: 500px) {
  .navbar a {
    float: none;
    display: block;
  }
}
</style>
<body>

<h2>Responsive Navigation Bar with Icons</h2>
<p>Try to resize the browser window to see the responsive effect.</p>

<div class="navbar">
  <a class="active" href="#1"><i class="fa fa-fw fa-home"></i> Home</a> 
  <a href="#2"><i class="fa fa-fw fa-search"></i> Search</a> 
  <a href="#3"><i class="fa fa-fw fa-envelope"></i> Contact</a> 
  <a href="#4"><i class="fa fa-fw fa-user"></i> Login</a>
</div>
<div id="1" style="background: red; height:1000px"></div>
<div id="2" style="background: green; height:1000px"></div>
<div id="3" style="background: blue; height:1000px"></div>
<div id="4" style="background: yellow; height:1000px"></div>
</body>
</html> 
